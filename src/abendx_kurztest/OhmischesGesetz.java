package abendx_kurztest;

// Aufgabe 4
public class OhmischesGesetz {
	private double strom;
	private double widerstand;
	private double leistung;
	private static double SPANNUNG = 230;

	public double getStrom() {
		return strom;
	}
	public double getWiederstand() {
		return widerstand;
	}
	public double getLeistung() {
		return leistung;
	}
	public double getSpannung() {
		return SPANNUNG;
	}
	public double berechneStrom(double widerstand) {
		strom = SPANNUNG / widerstand;
		return strom;
	}
	public double berechneWiderstand(double strom) {
		widerstand = SPANNUNG / strom;
		return widerstand;
	}
	public double berechneLeistung() {
		if (strom != 0d) {
			leistung = SPANNUNG * strom;
		}
		else if (widerstand != 0d) {
			leistung = (SPANNUNG * SPANNUNG) / widerstand;
		}
		else leistung = 0d;
		return leistung;
	}
}
