package abendx_kurztest;

public class main {

	public static void main(String[] args) {
		// Aufgabe 3
		System.out.println("Aufgabe 3:");
		double amount = konvertiereWaehrung(10.00d, "EUR");
		System.out.println(amount);
		System.out.println();
		
		// Aufgabe 4
		System.out.println("Aufgabe 4:");
		OhmischesGesetz ohmischesGesetz = new OhmischesGesetz();
		System.out.println(ohmischesGesetz.berechneStrom(5d));
		System.out.println(ohmischesGesetz.berechneWiderstand(10d));
		System.out.println(ohmischesGesetz.berechneLeistung());
		System.out.println(ohmischesGesetz.getStrom());
		System.out.println(ohmischesGesetz.getWiederstand());
		System.out.println(ohmischesGesetz.getLeistung());
		System.out.println(ohmischesGesetz.getSpannung());
		System.out.println();

		// Aufgabe 5
		System.out.println("Aufgabe 5:");
		whileToFor();
		System.out.println();

		// Aufgabe 6
		System.out.println("Aufgabe 6:");
		implementationSchleife(0);
		implementationSchleife(100);
		implementationSchleife(10000);
		System.out.println();
		
		// Aufgabe 7
		System.out.println("Aufgabe 7:");
		doWhileToFor(0);
		doWhileToFor(1);
		doWhileToFor(10);
		System.out.println();
	}
	
	// Aufgabe 3
	private static double konvertiereWaehrung (double amount, String currency) {
		double mod = 1;
		switch(currency) {
		case "EUR":
			mod = 0.90d;
			break;
		case "USD":
			mod = 1.06d;
			break;
		case "JPY":
			mod = 110.50d;
			break;
		case "AUD":
			mod = 1.47d;
			break;
		case "GBP":
			mod = 0.80d;
			break;
		case "NOK":
			mod = 9.35d;
			break;
		}
		return (amount * mod);
	}

	// Aufgabe 5
	private static void whileToFor() {
		for (int i = 1; i < 33000; i*=2) {
			  System.out.println(i);
		}
	}
	
	// Aufgabe 6
	private static void implementationSchleife(int n) {
		boolean prim = true;
		System.out.println("Primzahlen bis " + n + ": ");
		for (int i = 1; i < n; i++) {
			for (int j = 2; j < i -1; j++) {
				if (i % j == 0) prim = false;
			}
			if (prim) System.out.println(i);
			else prim = true;
		}
	}

	// Aufgabe 7
	private static void doWhileToFor(int i) {
		System.out.println("i = " + i);
		boolean runAtLeatOnce = true;
		for (int j = 0; j < i || runAtLeatOnce; j++) {
			runAtLeatOnce = false;
			double kreisinhalt = Math.round((Math.PI * Math.pow(j, 2)) * 1000.0) / 1000.0;
			System.out.println("Der Kreisinhalt von " + j + "cm Radius ist " + kreisinhalt + "cm2");
		}
	}
}