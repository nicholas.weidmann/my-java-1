package kurztest2uebung;

public class EnumUebung {
	public enum GueltigeWaehrungen {
		EUR(1.20),
		USD(1.01),
		GBP(1.53);
		
		public final double wechselkurs;
		
		private GueltigeWaehrungen(double wechselkurs) {
			this.wechselkurs = wechselkurs;
		}
	}

	public static void main(String[] args) {
		System.out.println(waehrungsUmrechnung(GueltigeWaehrungen.EUR, 10d));
		System.out.println(waehrungsUmrechnung(GueltigeWaehrungen.USD, 10d));
		System.out.println(waehrungsUmrechnung(GueltigeWaehrungen.GBP, 10d));
	}
	
	private static double waehrungsUmrechnung(GueltigeWaehrungen waehrung, double betrag) {
		return waehrung.wechselkurs * betrag;
	}

}
