package kurztest2uebung;

public class CaesarChiffre {

	public static void main(String[] args) {
		caesarChiffreUp("haus", 1);
		caesarChiffreUp("bauzaun", 8);
	}
	
	private static void caesarChiffreUp(String s1, int up) {
		if (up < 0 || up > 26) return;
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s1.length(); i++) {
			char ch = s1.charAt(i);
			char newCh = (char) ((int) ch + up);
			if (newCh >= 123) newCh -= 26;
			sb.append(newCh);
		}
		System.out.println(sb.toString());
	}
}
