package kurztest2uebung;

public class StringBufferUebung {

	public static void main(String[] args) {
		String[] texte1 = {"text zeile 1", "text zeile 2", "text zeile 3"};
		wandleZeichenkette(texte1);
		String[] texte2 = {"dies ist ", "ein ganz", " normaler ", "text"};
		wandleZeichenkette(texte2);
	}
	
	
	private static void wandleZeichenkette(String[] texte) {
		System.out.println("Wandle Zeichenkette");
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < texte.length; i++) {
			sb.append(texte[i]);
		}
		sb.append(texte[0]);
		System.out.println(sb.toString());
		

		for (int i = 0; i < texte.length; i++) {
			System.out.println(texte[i]);
		}
		System.out.println(texte[0]);
		System.out.println();
	}
}
