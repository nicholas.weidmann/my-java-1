package kurztest2uebung;

public class ZeichenZaehlen {

	public static void main(String[] args) {
		char[] zeichen1 = {'a', 'f', 'r'};
		String text1 = "dies ist ein normaler text";
		zaehleBestimmteZeichen(zeichen1, text1);
		char[] zeichen2 = {'a', 'g', 'i', 'o'};
		String text2 = "wie viele Buchstaben gibt es?";
		zaehleBestimmteZeichen(zeichen2, text2);
	}
	
	private static void zaehleBestimmteZeichen(char[] zeichen, String text) {
		System.out.println("Es wird folgender Text analysiert: " + text);
		
		for (int i = 0; i < zeichen.length; i++) {
			int count = 0;
			for (int y = 0; y < text.length(); y++) {
				if (text.charAt(y) == zeichen[i]) count++;
			}
			System.out.println("Anzahl " + zeichen[i] + ": " + count);
		}
		
		System.out.println();
	}
}
