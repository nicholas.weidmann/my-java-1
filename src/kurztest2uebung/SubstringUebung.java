package kurztest2uebung;

public class SubstringUebung {

	public static void main(String[] args) {
		String text1 = "dies ist ein text mit leerzeichen";
		String teilText1 = liefereSubstring(text1);
		String text2 = "diesIstEinTextOhneLeerzeichen";
		String teilText2 = liefereSubstring(text2);
	}

	private static String liefereSubstring(String text) {
		System.out.println("Liefere Substring");
		String substring = "";
		int index = text.indexOf(" ");
		

		if (index != -1) {
			substring = text.substring(index);
		}

		System.out.println(substring);
		System.out.println();
		return substring;
	}
}
