package t2;

public class Datatypes {

	public static void main(String[] args) {
		/*
		int dist = 100; // declaration
		System.out.println("Distanz = " + dist); // console output
		String isches = "isch das eso?";
		System.out.println(Double.MAX_VALUE);

int u;
int v;

u = 1;
v = u++;
System.out.println("u++");
System.out.println("u = " + u);
System.out.println("v = " + v);

u = 1;
v = ++u;
System.out.println("++u");
System.out.println("u = " + u);
System.out.println("v = " + v);

u = 1;
v = u+1;
System.out.println("u+1");
System.out.println("u = " + u);
System.out.println("v = " + v);
		*/

System.out.println("&&");
System.out.println(true && false);
System.out.println(true && true);
System.out.println(true && true && true && false && true && true);
System.out.println("");

System.out.println("||");
System.out.println(true || false);
System.out.println(true || true);
System.out.println(false || false || false || true || false);
System.out.println("");

System.out.println("^");
System.out.println(true ^ false);
System.out.println(true ^ true);
System.out.println(false ^ false);
System.out.println("");

System.out.println("!");
System.out.println(!true);
System.out.println(!false);
System.out.println(!(true || false));
System.out.println("");
		
		
		
	}

}
