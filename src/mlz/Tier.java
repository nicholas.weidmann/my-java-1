package mlz;

public class Tier {
	private int gewicht;
	private int alter;
	private String fellFarbe;
	
	public Tier(int gewicht, int alter, String fellFarbe) {
		this.gewicht = gewicht;
		this.alter = alter;
		this.fellFarbe = fellFarbe;
	}
	
	public void rufen() {
		
	}

	public int getGewicht() {
		return gewicht;
	}

	public void setGewicht(int gewicht) {
		this.gewicht = gewicht;
	}

	public int getAlter() {
		return alter;
	}

	public void setAlter(int alter) {
		this.alter = alter;
	}

	public String getFellFarbe() {
		return fellFarbe;
	}

	public void setFellFarbe(String fellFarbe) {
		this.fellFarbe = fellFarbe;
	}
}
