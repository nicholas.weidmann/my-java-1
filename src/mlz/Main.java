package mlz;

public class Main {

	public static void main(String[] args) {
		// AUFGABE 3
		int A = 100;
		int B = 2;
		int C = 2;
		int D = 2;
		System.out.println(A++ - B * C);
		System.out.println(A % B % C * D);
		System.out.println(true && false || true && ! false);
		System.out.println();
		
		// AUFGABE 6 A
		Tier tier = new Tier(100,50,"grau");
		Katze katze = new Katze(10,10,"braun", 1);
		Maus maus = new Maus(1,2,"weiss");
		Maus maus2 = new Maus(1,2,"grau");
		tier.rufen();
		katze.rufen();
		maus.rufen();
		System.out.println();
		
		// AUFGABE 6 B
		System.out.println(katze.essen(maus));
		System.out.println(katze.essen(maus2));
		System.out.println();
		
		// AUFGABE 7
		int[] zahlen = {5, 3, 12, 46};
		System.out.println(Summe(zahlen));
		System.out.println();
		
		// AUFGABE 8
		System.out.println(gemeinsameEndung("Tischlerei", "Fischerei"));
		System.out.println(gemeinsameEndung("Abakus", "Zirkus"));
	}

	private static int Summe(int[] intArray) {
		int summe = 0;
		for(int i = 0; i < intArray.length; i++) {
			summe += intArray[i];
		}
		return summe;
	}
	
	private static String gemeinsameEndung(String s1, String s2) {
		String endung = "";
		
		for (int i = 0; i < s1.length(); i++) {
			//System.out.println(s1.substring(s1.length()-i));
			//System.out.println(s2.substring(s2.length()-i));
			//System.out.println();
			if(s2.length()-i < 0) {
				// avoid out of bounds!!
				break;
			}
			if (s1.substring(s1.length()-i).equals(s2.substring(s2.length()-i))) {
				endung = s1.substring(s1.length()-i);

				//System.out.println("here");
			}
		}
		
		return endung;
	}
}
