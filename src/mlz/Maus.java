package mlz;

public class Maus extends Tier {
	private String lieblingsKaese;
	
	public Maus(int gewicht, int alter, String fellFarbe) {
		super(gewicht, alter, fellFarbe);
	}
	
	public void rufen() {
		System.out.println("Beep");
	}

	public String getLieblingsKaese() {
		return lieblingsKaese;
	}

	public void setLieblingsKaese(String lieblingsKaese) {
		this.lieblingsKaese = lieblingsKaese;
	}
}
