package mlz;

public class Katze extends Tier {
	private Maus[] magen;
	
	public Katze(int gewicht, int alter, String fellFarbe, int magenGroesse) {
		super(gewicht, alter, fellFarbe);
		this.magen = new Maus[magenGroesse];
	}
	
	public void rufen() {
		System.out.println("Miau");
	}
	
	public int essen(Maus maus) {
		int magenInhalt = 0;
		for (int i = 0; i < magen.length; i++) {
			magenInhalt = i;
			if (magen[i] == null) {
				System.out.println("isnull");
				magen[i] = maus;
				break;
			}
		}
		
		return magenInhalt + 1;
	}

	public Maus[] getMagen() {
		return magen;
	}

	public void setMagen(Maus[] magen) {
		this.magen = magen;
	}
	
}
