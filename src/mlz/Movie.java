package mlz;

public class Movie {
	private String title;
	private int year;
	private String genre;
	private static String[] categories = { "Drama", "Comedy",
		"Children", "Family", "Action", "Sci-Fi", "Documentary",
		"Other" };
	private static long moviesSold;
	
	public Movie() {
		title = "unknown";
	}
	
	public static String[] getCategories() {
		return categories;
	}
	
	public String getGenre() {
		return genre;
	}
	
	public Movie(String title, int year, String genre) {
		this.title = title;
		this.year = year;
		this.genre = genre;
	}
}
