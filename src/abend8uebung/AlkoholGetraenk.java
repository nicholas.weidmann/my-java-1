package abend8uebung;

public class AlkoholGetraenk extends Getraenk {
	private float alkoholgehalt = 0.1f;

	public float getAlkoholgehalt() {
		return alkoholgehalt;
	}

	public void setAlkoholgehalt(float alkoholgehalt) {
		this.alkoholgehalt = alkoholgehalt;
	}

}
