package abend8uebung;

public class Main {

	public static void main(String[] args) {
		Getraenk[] getraenke = new Getraenk[3];
		getraenke[0] = new SuessGetraenk();
		getraenke[1] = new AlkoholGetraenk();
		getraenke[2] = new AlkoholGetraenk();
		
		
		for (int i = 0; i < getraenke.length; i++) {
			if (getraenke[i] instanceof AlkoholGetraenk) {
				AlkoholGetraenk alkoholGetraenk = (AlkoholGetraenk) getraenke[i];
			} else {
				System.out.println(((AlkoholGetraenk) getraenke[i]).getAlkoholgehalt());
			}
		}
	}
}
