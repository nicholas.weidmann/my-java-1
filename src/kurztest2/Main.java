package kurztest2;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		// Aufgabe 1 a
		int[] zahlen = new int[4];
		zahlen = new int[]{50, 99, 120, 234};
		
		// Aufgabe 1 b
		int[] zahlen2 = {50, 99, 120, 234};
		System.out.println(zahlen2.length);
		
		// Aufgabe 2
		int[] zahl = {50, 99, 120, 234};
		for (int i = 0; i < zahl.length; i++) {
			System.out.println(zahl[i]);
		}
		
		// Aufgabe ab
		int[][] tabelle1 = new int[3][4];
		System.out.println(tabelle1[1][2]);
		
		// Aufabe c
		int[][] tabelle = { { 1001, 5, 500, 0 }, { 2001, 23, 1200, 0 }, { 3001, 2, 85, 0 } };
		for (int i = 0; i < tabelle.length; i++) {
			tabelle[i][3] = tabelle[i][1] * tabelle[i][2];
			System.out.println(tabelle[i][1] + "x Artikel Nr. " + tabelle[i][0] + " � " +
			tabelle[i][2] + " EUR = " + tabelle[i][3]);
		}
		
		// Aufgabe 4
		// Jahreszeiten.java
		
		// Aufgabe 5
		String a = new String("Test");
		String b = new String("Test");
		if (a == b) {
			System.out.println("Der Inhalt ist identisch!!!");
		} else {
			System.out.println("Der Inhalt ist unterschiedlich!!!");
		}
		
		// Aufgabe 6
		StringBuffer stb = new StringBuffer("Rolf ist alt!");
		stb.insert(9, "18 Jahre ");
		System.out.println(stb.toString());
	}

}
